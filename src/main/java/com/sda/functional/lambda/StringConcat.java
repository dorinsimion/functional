package com.sda.functional.lambda;

public interface StringConcat {
    String concat(String a,String b);
}
