package com.sda.functional.lambda;

@FunctionalInterface
public interface TestEqual {
    boolean test(int x,int y);
}
