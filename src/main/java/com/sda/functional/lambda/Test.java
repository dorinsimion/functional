package com.sda.functional.lambda;

@FunctionalInterface
public interface Test {
    boolean test(String str);
}
