package com.sda.functional.lambda;

public class TestStringEmpty implements Test {
    @Override
    public boolean test(String str) {
        return str.isEmpty();
    }
}
