package com.sda.functional.lambda;

@FunctionalInterface
public interface TestString {
    boolean testStrings(String str,String s);
}
