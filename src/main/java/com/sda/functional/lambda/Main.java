package com.sda.functional.lambda;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Test test = new TestStringContains();
        Test testEmpty = new TestStringEmpty();

        System.out.println(test.test("assAA"));
        System.out.println(testEmpty.test("s"));

        Test testAnonim = new Test() {
            @Override
            public boolean test(String str) {
                return str.isEmpty();
            }
        };
        System.out.println(testAnonim.test(""));

        Test lambda = (str) -> str.isEmpty();
        System.out.println(lambda.test("A"));

        TestEqual noEquals= (x,y) -> x==y;
        System.out.println(noEquals.test(4,4));
        TestEqual notEquals= (x,y) -> x!=y;
        System.out.println(notEquals.test(4,4));
        testing((x,y)->x==y,4,4);
        testing((x,y)->x>=y,4,4);

        TestString testString = (x,y)-> x.startsWith(y);
        System.out.println(testString.testStrings("ana","an"));

        StringConcat str = (x,y)->x.concat(" ").concat(y);
        System.out.println(str.concat("ana","mere"));
   }

    public static void testing(TestEqual test, int x,int y){
        System.out.println(test.test(x,y));
    }
}
