package com.sda.functional.streams;

import com.sda.functional.lambda.Student;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamsDemo {
    public static void main(String[] args) {
        Stream<Integer> emptyStream = Stream.empty();
        Stream<String> strings = Stream.of("a","b","c");
        Stream<Integer> iterate = Stream.iterate(1, x -> x + 2);
        Stream<Integer> generate = Stream.generate(()-> new Random().nextInt(49)+1);
        List<Student> list = new ArrayList<>();
        Stream<Student> stream = list.stream();

        Stream<String> stringsStream = Stream.of("ABC","Abc","bdc");
//        stringsStream.forEach(System.out::println);
        long count = stringsStream.count();
        System.out.println(count);
        int first = emptyStream.findFirst().orElse(1);
        System.out.println(first);//1

        boolean a = strings.anyMatch(s -> s.startsWith("a"));
        System.out.println(a);
        boolean b = Stream.iterate(1, x -> x + 3).anyMatch(x -> x > 100);
        //infinite stream
//        boolean c = Stream.generate(()-> new Random().nextInt(2)).noneMatch(x->x>100);
        boolean d = Stream.generate(()-> new Random().nextInt(2)).limit(4).noneMatch(x->x>100);

        Optional<Integer> min = Stream.of(1, 2, 3).min((i1, i2) -> i1 - i2);
        List<Integer> collect = Stream.of(1, 2, 3).collect(Collectors.toList());

        Stream.of(1,2,3,4,5)
                .filter(x-> x%2==0)
                .forEach(System.out::println);
        System.out.println("******");
        Stream.of(1,2,3,4).map(x->x*2).forEach(System.out::print);
        System.out.println();
        Stream.of(3,2,1).sorted().forEach(System.out::print);
        System.out.println();
        Stream.of(3,3,3,2,2,1,0).distinct().forEach(System.out::print);
        Stream.of(1,2,3,4).skip(2).sorted().forEach(System.out::print);

        Stream.of(3,2,3,1,2,3,5,3,2,1)
                .distinct()
                .filter(x->x%2==1)
                .map(x->x*2)
                .map(x->x+1)
                .sorted()
                .forEach(System.out::println);

        Stream.of("abc","bcd","a")
                .map(String::length)
                .forEach(System.out::println);

        List<String> str = new ArrayList<>();
        str.add("a"); str.add("bb");str.add("ccc");
        List<Integer> c1 = str.stream()
                .map(String::length)
                .collect(Collectors.toList());
        Set<Integer> c2 = Stream.of(5, 2, 3)
                .collect(Collectors.toSet());
        Set<Integer> c3 = Stream.of(5, 2, 3)
                .collect(Collectors.toCollection(TreeSet::new));
        String concatenate = Stream.of("a", "b", "c")
                .collect(Collectors.joining(","));
        Map<Integer, String> map = Stream.of("a", "bcc", "ab")
                .collect(Collectors.toMap(k -> k.length(), v -> v));
        System.out.println(map);
        Map<Boolean, List<Integer>> c4 = Stream.of(1, 2, 3, 4, 5)
                .collect(Collectors.partitioningBy(x -> x % 2 == 0));
        Map<Integer, List<Integer>> c5 = Stream.of(-1,1,-2, 2, 3, 4, 5)
                .collect(Collectors.groupingBy(x -> x*x));
        System.out.println(c5);
        IntStream s = Stream.of(1,2,3).mapToInt(x->x);
//        Stream<Integer> objStream= s.mapToObj(x->x);
        int sum = s.sum();
        Integer reduce = Stream.of(1, 2, 3).reduce(0, (x1, x2) -> x1 + x2);
        OptionalDouble average = Stream.of(1, 2, 3).mapToInt(x -> x).average();
        double equals = average.orElse(0);
        String str2 = Stream.of("a", "B", "c").reduce("", String::concat);
        String str3 = Stream.of("a", "B").collect(Collectors.joining(","));
        System.out.println(str2+":"+str3);
        Integer result = Stream.of(1, 2, 3).collect(Collectors.summingInt(x -> x));


    }
}
